# DroneDeliveryService

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

## Condition
A squad of drones have been tasked with delivering packages for a major online reseller in a world where time and distance do not matter.  Each drone can carry a specific weight, and can make multiple deliveries before returning to home base to pick up additional loads; however the goal is to make the fewest number of trips as each time the drone returns to home base it is extremely costly to refuel and reload the drone.
The purpose of the written software will be to accept input which will include the name of each drone and the maximum weight it can carry, along with a series of locations and the total weight needed to be delivered to that specific location.  The software should highlight the most efficient deliveries for each drone to make on each trip.
Assume that time and distance to each drop off location do not matter, and that size of each package is also irrelevant.  It is also assumed that the cost to refuel and restock each drone is a constant and does not vary between drones.  The maximum number of drones in a squad is 100, and there is no maximum number of deliveries which are required.

#### Given Input
`Line 1: [Drone #1 Name], [#1 Maximum Weight], [Drone #2 Name], [#2 Maximum Weight], etc.`  
`Line 2: [Location #1 Name], [Location #1 Package Weight]`  
`Line 3: [Location #2 Name], [Location #2 Package Weight]`  
`Line 4: [Location #3 Name], [Location #3 Package Weight]`  
`Etc.`  

#### Expected Output
`[Drone #1 Name]`  
`Trip #1`  
`[Location #2 Name], [Location #3 Name]`  
`Trip #2`  
`[Location #1 Name]`  
  
`[Drone #2 Name]`  
`Trip #1`  
`[Location #4 Name], [Location #7 Name]`  
`Trip #2`  
`[Location #5 Name], [Location #6 Name]`  

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
