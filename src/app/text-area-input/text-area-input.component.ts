import { Component, OnInit } from '@angular/core';
import { DroneDeliveryService } from 'src/services/DroneDeliveryService';
import { ILocation } from 'src/interfaces/ILocation';
import { IDrone } from 'src/interfaces/IDrone';

@Component({
  selector: 'app-text-area-input',
  templateUrl: './text-area-input.component.html',
  styleUrls: ['./text-area-input.component.css']
})
export class TextAreaInputComponent implements OnInit {
  public textarea: string;
  public drones: IDrone[] = [];
  public locations: ILocation[] = [];

  constructor(private droneDeliveryService: DroneDeliveryService) { }

  ngOnInit() {
  }

  public parseTextarea() {
    const rows = this.textarea.split('\n');
    const dronesRaw = rows[0].split(',');

    if (dronesRaw.length > 200) {
      alert('Cannot insert more than 100 drones!');
      return;
    }

    for (let i = 0; i < dronesRaw.length; i += 2) {
      this.drones.push({name: dronesRaw[i], maxWeight: +dronesRaw[i + 1], locations: []});
    }
    for (let i = 1; i < rows.length; i++) {
      const loc = rows[i].split(',');
      this.locations.push({name: loc[0], packageWeight: +loc[1]});
    }
    this.droneDeliveryService.setDroneList(this.drones);
    this.droneDeliveryService.setLocationList(this.locations);
  }

}
