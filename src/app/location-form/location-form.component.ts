import { Component, OnInit } from '@angular/core';
import { ILocation } from 'src/interfaces/ILocation';
import { DroneDeliveryService } from 'src/services/DroneDeliveryService';

@Component({
  selector: 'app-location-form',
  templateUrl: './location-form.component.html',
  styleUrls: ['./location-form.component.css']
})
export class LocationFormComponent implements OnInit {
  public locations: ILocation[];
  public locationCompleted = false;

  constructor(private droneDeliveryService: DroneDeliveryService) {
  }

  ngOnInit() {
    this.locations = [{name: null, packageWeight: null}];
  }

  public addLocation() {
    this.locations.push({name: null, packageWeight: null});
  }

  public onSubmit() {
    this.locationCompleted = true;
    this.droneDeliveryService.setLocationList(this.locations);
  }
}
