import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { DroneFormComponent } from './drone-form/drone-form.component';
import { LocationFormComponent } from './location-form/location-form.component';
import { DroneDeliveryService } from 'src/services/DroneDeliveryService';
import { TextAreaInputComponent } from './text-area-input/text-area-input.component';

const appRoutes: Routes = [
  { path: 'text-area', component: TextAreaInputComponent },
  { path: '', component: DroneFormComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    DroneFormComponent,
    LocationFormComponent,
    TextAreaInputComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false }
    )
  ],
  providers: [
    DroneDeliveryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
