import { Component, OnInit } from '@angular/core';
import { IDrone } from 'src/interfaces/IDrone';
import { DroneDeliveryService } from 'src/services/DroneDeliveryService';

@Component({
  selector: 'app-drone-form',
  templateUrl: './drone-form.component.html',
  styleUrls: ['./drone-form.component.css']
})
export class DroneFormComponent implements OnInit {
  public drones: IDrone[];
  public droneCompleted = false;

  constructor(private droneDeliveryService: DroneDeliveryService) {
  }

  ngOnInit() {
    this.drones = [{name: null, maxWeight: null}];
  }

  public addDrone() {
    this.drones.push({name: null, maxWeight: null});
  }

  public onSubmit() {
    this.droneCompleted = true;
    this.drones.forEach(x => x.locations = []);
    this.droneDeliveryService.setDroneList(this.drones);
  }
}
