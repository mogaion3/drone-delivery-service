import { Component } from '@angular/core';
import { IDrone } from 'src/interfaces/IDrone';
import { DroneDeliveryService } from 'src/services/DroneDeliveryService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public title = 'drone-delivery-service';
  public dronesCb: Promise<IDrone[]>;

  constructor(private droneDeliveryService: DroneDeliveryService) {
  }

  public canCalculateTrips(): boolean {
    return this.droneDeliveryService.getDroneList().length > 0 && this.droneDeliveryService.getLocationList().length > 0;
  }

  public calculateTrips() {
    this.dronesCb = this.droneDeliveryService.calculateDroneTrips();
    this.dronesCb.then(() => setTimeout(() => window.scrollTo(0, document.body.scrollHeight)));
  }
}
