import { Injectable } from '@angular/core';
import { IDrone } from 'src/interfaces/IDrone';
import { ILocation } from 'src/interfaces/ILocation';

@Injectable()
export class DroneDeliveryService {
  private droneList: IDrone[] = [];
  private locationList: ILocation[] = [];

  public setDroneList(droneList: IDrone[]): void {
    this.droneList = Object.assign([], droneList);
  }

  public getDroneList(): IDrone[] {
    return this.droneList;
  }

  public setLocationList(locationList: ILocation[]): void {
    this.locationList = Object.assign([], locationList);
  }

  public getLocationList(): ILocation[] {
    return this.locationList;
  }

  public async calculateDroneTrips(): Promise<IDrone[]> {
    this.droneList = this.droneList.sort((a, b) => b.maxWeight - a.maxWeight);
    this.locationList = this.locationList.sort((a, b) => b.packageWeight - a.packageWeight);

    await this.prepare();

    return this.droneList;
  }

  private async prepare() {
    const component = this;

    const promises = this.droneList.map(async (drone) => {await component.preparePackage(drone); });

    await Promise.all(promises);
    if (this.locationList.filter(x => !x.isSent).length > 0) {
      await this.prepare();
    }
  }

  private preparePackage(drone: IDrone) {
    return new Promise(resolve => {
      setTimeout(() => {
        const dronePackages = [];
        let weight = drone.maxWeight;
        for (const pack of this.locationList.filter(x => !x.isSent)) {
          if (weight - pack.packageWeight < 0) {
            continue;
          } else {
            weight -= pack.packageWeight;
            pack.isSent = true;
            dronePackages.push(pack);
          }
        }
        if (dronePackages.length > 0) {
          drone.locations.push(dronePackages);
        }
        resolve();
      }, Math.floor(Math.random() * Math.floor(1000)));
    });
  }
}
