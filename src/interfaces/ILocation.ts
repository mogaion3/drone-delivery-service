export interface ILocation {
    name: string;
    packageWeight: number;
    isSent?: boolean;
}
