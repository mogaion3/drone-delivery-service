import { ILocation } from './ILocation';

export interface IDrone {
    name: string;
    maxWeight: number;
    locations?: ILocation[][];
}
